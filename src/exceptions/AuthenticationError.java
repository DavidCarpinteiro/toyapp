package exceptions;

public class AuthenticationError extends Exception {

    public AuthenticationError() {
        super();
    }

    public AuthenticationError(String msg) {
        super(msg);
    }
}
