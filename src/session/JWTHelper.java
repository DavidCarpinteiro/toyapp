package session;

import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.AuthenticationError;
import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public final class JWTHelper {
    private static final long EXPIRATION = Long.parseLong(Configs.get(Props.token_ttl));
    private static final String USER = "USER";

    public static String createJWT(IAccount account) {
        Objects.requireNonNull(account);

        final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        final long nowMillis = System.currentTimeMillis();
        final Date now = new Date(nowMillis);

        final byte[] apiKeySecretBytes = account.getKey().getEncoded();
        final Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        final Map<String, Object> header = new HashMap<>();
        header.put(Header.TYPE, Header.JWT_TYPE);
        header.put(JwsHeader.ALGORITHM, signatureAlgorithm);
        header.put(USER, account.getUsername());

        final JwtBuilder builder = Jwts.builder()
                .setHeader(header)
                .setId(account.getID())
                .setIssuedAt(now)
                .setExpiration(new Date(nowMillis + EXPIRATION))
                .signWith(signingKey);

        return builder.compact();
    }

    public static IAccount decodeJWT(String jwt) throws AuthenticationError {
        if (jwt == null) throw new AuthenticationError("NULL Token");

        try {
            /*
                Work around to parse the header without supplying a key
                This is needed to obtain the username, a then obtain the key
            */
            String[] splitToken = jwt.split("\\.");
            String unsignedToken = splitToken[0] + "." + splitToken[1] + ".";
            final Header header = Jwts.parser().parse(unsignedToken).getHeader();

            final String username = (String) header.get(USER);

            final IAccount account = IAuthenticator.get_account(username);

            if (account == null) {
                System.err.println("Username does not exist");
                throw new AuthenticationError();
            }

            final Key key = account.getKey();

            final Claims claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(jwt).getBody();

            if (!claims.getId().equals(account.getID())) {
                System.err.println("User ID does not match");
                throw new AuthenticationError();
            }

            return account;

        } catch (Exception e) {
            System.err.println("Could not Authenticate Token");
            e.printStackTrace();
            throw new AuthenticationError();
        }
    }

}
