package session;

import account.Account;
import account.IAccount;
import account.InputDataValidation;
import configs.Configs;
import configs.Props;
import exceptions.*;
import storage.AccountStorageMiddleware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;
import java.util.Optional;

public interface IAuthenticator {

    static void create_account(String name, String pwd1, String pwd2) throws PasswordsDoNotMatch, UsernameAlreadyExists, InvalidInput {

        if (AccountStorageMiddleware.get().hasAccount(name)) {
            throw new UsernameAlreadyExists();
        }

        if (!pwd1.equals(pwd2)) {
            throw new PasswordsDoNotMatch();
        }

        if(!( InputDataValidation.validateUsername(name) && InputDataValidation.validatePassword(pwd1)) ) {
            throw new InvalidInput();
        }

        Account account = new Account(name, pwd1);

        AccountStorageMiddleware.get().saveAccount(account);
    }


    static void delete_account(String name) throws AccountIsLogin, AccountNotLocked, AccountNotFound {
        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(name);

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        IAccount account = accountOptional.get();

        if (!account.isLocked()) {
            throw new AccountNotLocked();
        }

        if (account.isLogin()) {
            throw new AccountIsLogin();
        }

        AccountStorageMiddleware.get().deleteAccount(name);
    }


    static IAccount get_account(String name) throws AccountNotFound {
        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(name);

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        IAccount account = accountOptional.get();

        return account.convert();
    }


    static void change_pwd(String name, String pwd1, String pwd2) throws PasswordsDoNotMatch, AccountNotFound, InvalidInput {
        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(name);

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        Account account = accountOptional.get();

        if (!pwd1.equals(pwd2)) {
            throw new PasswordsDoNotMatch();
        }

        if(!(InputDataValidation.validatePassword(pwd1)) ) {
            throw new InvalidInput();
        }

        account.setPassword(pwd1);

        AccountStorageMiddleware.get().updateAccount(account);
    }


    static IAccount login(String name, String pwd) throws AccountNotFound, AccountIsLocked, AuthenticationError {
        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(name);

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        Account account = accountOptional.get();

        if (account.isLocked()) {
            throw new AccountIsLocked();
        }

        if (!account.checkPassword(pwd)) {
            throw new AuthenticationError();
        }

        account.setLogin(true);

        AccountStorageMiddleware.get().updateAccount(account);

        return account;
    }


    static void logout(IAccount acc) throws AccountNotFound {
        Objects.requireNonNull(acc, "Account can't be null at Logout");

        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(acc.getUsername());

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        Account account = accountOptional.get();

        account.setLogin(false);

        // Generating new key for Tokens, invalidating previous ones
        account.generateNewKey();

        AccountStorageMiddleware.get().updateAccount(account);
    }

    static void set_locked(IAccount acc) throws AccountNotFound {
        Objects.requireNonNull(acc, "Account can't be null at Locked");

        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(acc.getUsername());

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        Account account = accountOptional.get();

        account.setLocked(true);

        AccountStorageMiddleware.get().updateAccount(account);
    }

    static void set_unlocked(IAccount acc) throws AccountNotFound {
        Objects.requireNonNull(acc, "Account can't be null at Unlocked");


        Optional<Account> accountOptional = AccountStorageMiddleware.get().getAccount(acc.getUsername());

        if (accountOptional.isEmpty()) {
            throw new AccountNotFound();
        }

        Account account = accountOptional.get();

        account.setLocked(false);

        AccountStorageMiddleware.get().updateAccount(account);
    }


    static IAccount login(HttpServletRequest req, HttpServletResponse res) throws AuthenticationError {
        final HttpSession session = req.getSession(true);

        final String jwt = (String) session.getAttribute(Configs.get(Props.jwt));

        try {
            final IAccount account = JWTHelper.decodeJWT(jwt);

            System.out.println("User " + account.getUsername() + " authenticated");

            return account;
        } catch (AuthenticationError e) {
            session.invalidate();
            throw e;
        }
    }
}
