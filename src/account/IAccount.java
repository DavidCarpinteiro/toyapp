package account;

import java.io.Serializable;
import java.security.Key;
import java.util.Date;

public interface IAccount extends Serializable {

    String getUsername();

    String getPassword();

    void setPassword(String password);

    String getID();

    boolean isLocked();

    void setLocked(boolean locked);

    boolean isLogin();

    void setLogin(boolean login);

    IAccount convert();

    Date getCreation();

    Date getModified();

    boolean checkPassword(String password);

    Key getKey();

    void generateNewKey();
}
