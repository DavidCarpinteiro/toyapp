package account;

import java.util.Objects;

public class InputDataValidation {

    public static boolean validateUsername(String username) {
        Objects.requireNonNull(username);

        return username.matches("^[a-zA-Z_]{4,20}$");
    }

    public static boolean validatePassword(String password) {
        Objects.requireNonNull(password);

        return password.matches("^[a-zA-Z0-9]{4,20}$");
    }
}
