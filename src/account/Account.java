package account;

import configs.Configs;
import configs.Props;
import utils.Hash;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public final class Account implements IAccount {
    private final String username;
    private final Date creation;
    private final String id;
    private byte[] key;
    private String password;
    private boolean login;
    private boolean locked;
    private Date modified;

    public Account(String username, String password) {
        Objects.requireNonNull(username, "Username can't be NULL!");
        Objects.requireNonNull(password, "Password can't be NULL!");

        this.id = this.genID();
        this.username = username;
        this.password = this.hashPassword(password);
        this.login = false;
        this.locked = false;
        this.creation = new Date();
        this.modified = new Date();
        this.key = this.genKey();
    }

    private String genID() {
        return UUID.randomUUID().toString();// new SecureRandom().nextLong();
    }

    private String hashPassword(String password) {
        return Hash.digestHex(password, id.getBytes(Charset.defaultCharset()));
    }

    private byte[] genKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(Configs.get(Props.key_alg));
            keyGen.init(Integer.parseInt(Configs.get(Props.key_size)));
            return keyGen.generateKey().getEncoded();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error generating key...");
        }
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        Objects.requireNonNull(password, "Password can't be NULL!");

        this.password = this.hashPassword(password);
        setModified();
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public boolean isLocked() {
        return this.locked;
    }

    @Override
    public void setLocked(boolean locked) {
        this.locked = locked;
        setModified();
    }

    @Override
    public boolean isLogin() {
        return this.login;
    }

    @Override
    public void setLogin(boolean login) {
        this.login = login;
        setModified();
    }

    @Override
    public IAccount convert() {
        return new AccountReadonly(this);
    }

    @Override
    public boolean checkPassword(String password) {
        Objects.requireNonNull(password, "Password can't be NULL!");

        String cmp_pass = this.hashPassword(password);

        return cmp_pass.equals(this.password);
    }

    @Override
    public Key getKey() {
        return new SecretKeySpec(this.key, 0, this.key.length, Configs.get(Props.key_alg));
    }

    @Override
    public void generateNewKey() {
        this.key = genKey();
    }

    @Override
    public Date getCreation() {
        return creation;
    }

    @Override
    public Date getModified() {
        return modified;
    }

    private void setModified() {
        this.modified = new Date();
    }
}
