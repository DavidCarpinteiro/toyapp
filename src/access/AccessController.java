package access;

import account.IAccount;
import exceptions.AccessControlError;
import storage.RolesAccountStorageMiddleware;
import storage.RolesIDStorageMiddleware;

import java.util.Objects;

public final class AccessController {

    public static Role newRole(Role.ROLE_ID role_id) {
        Objects.requireNonNull(role_id, "ID cannot be null");

        Role role = new Role(role_id);
        RolesIDStorageMiddleware.get().put(role_id, role);

        return role;
    }

    public static void setRole(IAccount user, Role role) {
        Objects.requireNonNull(user, "Account cannot be null");
        Objects.requireNonNull(role, "Role cannot be null");

        RolesAccountStorageMiddleware.get().put(user, role.getId());
    }

    public static Role getRole(IAccount user) {
        Objects.requireNonNull(user, "Account cannot be null");

        Role.ROLE_ID role_id = RolesAccountStorageMiddleware.get().get(user);

        return RolesIDStorageMiddleware.get().get(role_id);
    }

    public static Role getRole(Role.ROLE_ID role_id) {
        Objects.requireNonNull(role_id, "ID cannot be null");

        return RolesIDStorageMiddleware.get().get(role_id);
    }

    public static void grantPermission(Role role, Resource res, Operation op) {
        role.addPermission(res, op);
        RolesIDStorageMiddleware.get().put(role.getId(), role);
    }

    public static void revokePermission(Role role, Resource res, Operation op) {
        role.removePermission(res, op);
        RolesIDStorageMiddleware.get().put(role.getId(), role);
    }

    public static Capability makeKey(Role role) {
        Objects.requireNonNull(role, "Role Cannot be null");

        return new Capability(role.getPermissions());
    }

    public static void checkPermission(Capability cap, Resource res, Operation op) throws AccessControlError {

        boolean result = cap.hasPermission(res, op);

        if (!result)
            throw new AccessControlError();
    }

}
