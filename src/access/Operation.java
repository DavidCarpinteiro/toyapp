package access;

public enum Operation {

    CREATE,
    DELETE,
    MODIFY,
    READ,
    LOCK

}
