package access;


import java.io.Serializable;
import java.util.*;

public final class Role implements Serializable {

    private ROLE_ID id;

    public enum ROLE_ID {
        BASIC,
        ROOT
    }

    private final Map<Resource, List<Operation>> permissions;

    Role(ROLE_ID id) {
        this.id = id;
        permissions = new HashMap<>();
    }

    public ROLE_ID getId() {
        return id;
    }

    Map<Resource, List<Operation>> getPermissions() {
        // Create a new copy of permissions
        return new HashMap<>(permissions);
    }


    void addPermission(Resource res, Operation ope) {
        Objects.requireNonNull(res, "Resource cannot be null");
        Objects.requireNonNull(ope, "Operation cannot be null");

        List<Operation> operations = permissions.get(res);

        operations = Objects.requireNonNullElse(operations, new LinkedList<>());

        operations.add(ope);

        permissions.putIfAbsent(res, operations);
    }


    void removePermission(Resource res, Operation ope) {
        Objects.requireNonNull(res, "Resource cannot be null");
        Objects.requireNonNull(ope, "Operation cannot be null");

        List<Operation> operations = permissions.get(res);

        if (operations == null)
            return;

        operations.remove(ope);
    }

}
