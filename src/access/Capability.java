package access;

import java.util.List;
import java.util.Map;

public final class Capability {

    private final Map<Resource, List<Operation>> permissions;


    Capability(Map<Resource, List<Operation>> permissions) {
        this.permissions = permissions;
    }

    boolean hasPermission(Resource res, Operation op) {

        List<Operation> operations = permissions.get(res);

        if(operations == null)
            return false;

        return operations.contains(op);
    }
}
