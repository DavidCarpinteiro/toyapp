package utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Hash {

    // private static final String ALG = "AES";
    // private static final byte[] keyValue = new byte[]{'F', 'C', 'T', '/', 'U', 'N', 'L', 'r', 'o', 'c', 'k', 's', '!', '!', 'd', 'i'};
    // private Key apiKey = new SecretKeySpec(keyValue, ALG);

    private static byte[] digestBytes(String val, byte[] salt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(salt);
            return digest.digest(val.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Could not digest...");
        }
    }

    public static String digestHex(String val, byte[] salt) {
        return bytesToHex(digestBytes(val, salt));
    }

    private static String bytesToHex(byte[] hash) {
        final StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            final String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

/*    public String encrypt(String Data) throws Exception {
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.ENCRYPT_MODE, apiKey);
        byte[] encVal = c.doFinal(Data.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }

    public String decrypt(String encrypted) throws Exception {
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.DECRYPT_MODE, apiKey);
        byte[] decodedValue = Base64.getDecoder().decode(encrypted);
        byte[] decValue = c.doFinal(decodedValue);
        return new String(decValue);
    }*/
}
