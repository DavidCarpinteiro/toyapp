package storage;

import access.AccessController;
import access.Operation;
import access.Resource;
import access.Role;
import account.Account;
import configs.Configs;
import configs.Props;
import storage.database.IDatabase;
import storage.database.local.DatabaseAccessFactory;

import java.util.Objects;
import java.util.Optional;

public final class AccountStorageMiddleware {
    private static AccountStorageMiddleware instance;
    private final IDatabase<String, Account> database;

    private AccountStorageMiddleware() {
        //DatabaseAccessFactory.<String, IAccount>get()
        database = DatabaseAccessFactory.get("accounts");
    }

    public static AccountStorageMiddleware get() {
        if (instance == null) {
            instance = new AccountStorageMiddleware();

            if (instance.isNew()) {
                System.out.println("Adding base user and Permissions");
                // If db files get deleted
                Account acc = new Account(Configs.get(Props.admin), Configs.get(Props.pass));
                Role role = AccessController.newRole(Role.ROLE_ID.ROOT);
                AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.MODIFY);
                AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.MODIFY);
                AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.READ);
                AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.CREATE);
                AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.DELETE);
                AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.READ);
                AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.LOCK);
                AccessController.setRole(acc, role);
                instance.saveAccount(acc);
                // Setup permission for basic users
                role = AccessController.newRole(Role.ROLE_ID.BASIC);
                AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.MODIFY);
                AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.READ);
            }
        }


        return instance;
    }

    private boolean isNew() {
        return database.isNewFile();
    }

    public void updateAccount(Account account) {
        Objects.requireNonNull(account);
        database.update(account.getUsername(), account);
    }

    public void saveAccount(Account account) {
        Objects.requireNonNull(account);
        database.add(account.getUsername(), account);
    }

    public Optional<Account> getAccount(String username) {
        Objects.requireNonNull(username);
        Account account = database.get(username);
        return account != null ? Optional.of(account) : Optional.empty();
    }

    public void deleteAccount(String username) {
        Objects.requireNonNull(username);
        database.delete(username);
    }

    public boolean hasAccount(String username) {
        Objects.requireNonNull(username);
        return database.exists(username);
    }


}
