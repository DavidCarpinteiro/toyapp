package storage;

import access.Role;
import storage.database.IDatabase;
import storage.database.local.DatabaseAccessFactory;

public class RolesIDStorageMiddleware {
    private static RolesIDStorageMiddleware instance;
    private final IDatabase<String, Role> database;

    private RolesIDStorageMiddleware() {
        database = DatabaseAccessFactory.get("roles_id");
    }

    public static RolesIDStorageMiddleware get() {
        if (instance == null) {
            instance = new RolesIDStorageMiddleware();
        }

        return instance;
    }

    public void put(Role.ROLE_ID role_id, Role role) {
        database.add(role_id.name(), role);
    }

    public Role get(Role.ROLE_ID role_id) {
        return database.get(role_id.name());
    }

}
