package storage.database.local;

import storage.database.IDatabase;

public class DatabaseAccessFactory {

    @SuppressWarnings("unchecked")
    public static <K, V> IDatabase<K, V> get(String name) {
        return new LocalDatabase<>(name);
    }

}
