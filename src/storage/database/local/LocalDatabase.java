package storage.database.local;

import com.google.gson.Gson;
import storage.database.IDatabase;

import java.io.*;
import java.util.HashMap;
import java.util.Map;


final class LocalDatabase<K, V> implements IDatabase<K, V> {
    private final Gson gson;
    private final File file;
    private Map<K, V> data;
    private boolean newFile = true;

    LocalDatabase(String name) {
        // Fixed path was the only to get this to work, change accordingly
        final String filePath = "C:\\Users\\david\\Dropbox\\SegSoft\\ToyApp\\db\\data_" + name + ".ser";

        //AccessController.checkPermission(new FilePermission(filePath, "write"));

        gson = new Gson();

        this.file = new File(filePath);

        final boolean dir_created = this.file.getParentFile().mkdirs();

        if (dir_created) {
            System.out.println("Directory (" + filePath + ") was created");
        }

        readFile();
    }

    public boolean isNewFile() {
        return newFile;
    }

    @Override
    public void update(K key, V value) {
        data.put(key, value);

        saveFile();
    }

    @Override
    public void delete(K key) {
        data.remove(key);

        saveFile();
    }

    @Override
    public V get(K key) {
        return data.get(key);
    }

    @Override
    public void add(K key, V value) {
        data.put(key, value);

        saveFile();
    }

    @Override
    public boolean exists(K key) {
        return data.containsKey(key);
    }

    private void readFile() {
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = (Map<K, V>) ois.readObject();

            System.out.println("Data Loaded:");
            data.forEach((k, v) -> System.out.println("\t" + k));

            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Creating new file...");
            data = new HashMap<>();
            newFile = true;
        }

/*        try {
            final Type REVIEW_TYPE = new TypeToken<HashMap<K, V>>() {
            }.getType();

            final JsonReader reader = new JsonReader(new FileReader(file));
            data = gson.fromJson(reader, REVIEW_TYPE);

            System.out.println("Data Loaded:");
            data.forEach((k, v) -> System.out.println("\t" + k));

        } catch (FileNotFoundException e) {
            System.out.println("Creating new file...");
            data = new HashMap<>();
        }*/
    }

    private void saveFile() {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
        } catch (IOException e) {
            System.err.println("Error saving file!");
            e.printStackTrace();
        }

        /*final String json = gson.toJson(data);

        try {
            gson.newJsonWriter(new FileWriter(file)).jsonValue(json).flush();
        } catch (IOException e) {
            System.err.println("Error saving file!");
            e.printStackTrace();
        }*/
    }
}
