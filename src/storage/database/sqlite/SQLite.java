package storage.database.sqlite;

import account.Account;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.File;
import java.sql.*;
import java.util.Objects;

public class SQLite {
    private static final String DATABASE_FILE = "sqlite/data.db";
    private static final String url = "jdbc:sqlite:" + DATABASE_FILE;

    static {
        System.out.println("Working Directory = " + System.getProperty("account.dir"));
        System.out.println("LOADING");

        try {
            File folder = new File("src/sqlite");
            File[] listOfFiles = folder.listFiles();
            for (File listOfFile : Objects.requireNonNull(listOfFiles)) {
                if (listOfFile.isFile()) {
                    System.out.println("File " + listOfFile.getName());
                } else if (listOfFile.isDirectory()) {
                    System.out.println("Directory " + listOfFile.getName());
                }
            }
        } catch (Exception e) {
            System.out.println("1");
        }
        try {
            File folder = new File("sqlite");
            File[] listOfFiles = folder.listFiles();

            for (File listOfFile : Objects.requireNonNull(listOfFiles)) {
                if (listOfFile.isFile()) {
                    System.out.println("File " + listOfFile.getName());
                } else if (listOfFile.isDirectory()) {
                    System.out.println("Directory " + listOfFile.getName());
                }
            }
        } catch (Exception e) {
            System.out.println("1");
        }

        //createNewDatabase();
        dropTable();
        createNewTable();
        insertAccount("root", "root", false, false);
    }

/*
    public static void main(String[] args) {
        SQLite sq = new SQLite();
        sq.getAccount("asdasd");
        System.out.println("Here");
        sq.dropTable();
        sq.createNewTable();
        sq.insertAccount("asd", "1234", true, false);
        sq.insertAccount("asdasd", "12345", true, true);

        sq.getAccount("asd");
        sq.deleteAccount("asd");
        Values v = sq.getAccount("asd");

        sq.update("david2", "0000", false, false);
        sq.getAccount("asdasd");
    }
*/

    public static void updateAccount(String username, String password, boolean login, boolean locked) {
        String sql = "UPDATE accounts SET password = ? , "
                + "login = ? , "
                + "locked = ? "
                + "WHERE username = ?";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, password);
            pstmt.setInt(2, login ? 1 : 0);
            pstmt.setInt(3, locked ? 1 : 0);
            pstmt.setString(4, username);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void deleteAccount(String username) {
        String sql = "DELETE FROM accounts WHERE username = ?";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, username);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Account getAccount(String username) {
        String sql = "SELECT DISTINCT * FROM accounts where username = ?";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, username);

            ResultSet rs = pstmt.executeQuery();

            return null;//new Values(rs.getString("username"), rs.getString("password"), rs.getInt("login"), rs.getInt("locked"));


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public static void insertAccount(String username, String password, boolean login, boolean locked) {
        String sql = "INSERT INTO accounts(username,password,login,locked) VALUES(?,?,?,?)";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setInt(3, login ? 1 : 0);
            pstmt.setInt(4, locked ? 1 : 0);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void createNewTable() {

        String sql = "CREATE TABLE IF NOT EXISTS accounts ("
                + " username text PRIMARY KEY,"
                + " password text NOT NULL,"
                + " login integer,"
                + " locked integer"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {

            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void dropTable() {
        String sql = "DROP TABLE IF EXISTS accounts";

        try (Connection conn = connect();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection connect() {
        Connection conn = null;
        /*
        try {
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } /*finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }*/

        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/sqlite");
            conn = ds.getConnection();
        } catch (SQLException | NamingException se) {
            se.printStackTrace();
        }

        return conn;
    }

    private static void createNewDatabase() {

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new storage.database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
