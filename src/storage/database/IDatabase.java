package storage.database;

public interface IDatabase<K, V> {

    void update(K key, V value);

    void delete(K key);

    V get(K key);

    void add(K key, V value);

    boolean exists(K key);

    boolean isNewFile();
}
