package storage;

import account.IAccount;
import access.Role;
import storage.database.IDatabase;
import storage.database.local.DatabaseAccessFactory;

public class RolesAccountStorageMiddleware {
    private static RolesAccountStorageMiddleware instance;
    private final IDatabase<String, Role.ROLE_ID> database;

    private RolesAccountStorageMiddleware() {
        database = DatabaseAccessFactory.get("roles_account");
    }

    public static RolesAccountStorageMiddleware get() {
        if (instance == null) {
            instance = new RolesAccountStorageMiddleware();
        }

        return instance;
    }

    public void put(IAccount user, Role.ROLE_ID role_id) {
        database.add(user.getID(), role_id);
    }

    public Role.ROLE_ID get(IAccount user) {
        return database.get(user.getID());
    }
}
