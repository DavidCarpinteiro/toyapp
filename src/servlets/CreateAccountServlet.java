package servlets;

import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.*;
import access.*;
import session.IAuthenticator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreateAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 98243798327L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT CREATE ACCOUNT");

        String name = req.getParameter(Configs.get(Props.username));
        String pwd = req.getParameter(Configs.get(Props.password));
        String pwd2 = req.getParameter(Configs.get(Props.password_conf));

        try {
            IAccount authUser = IAuthenticator.login(req, res);

            Role role = AccessController.getRole(authUser);
            Capability cap = AccessController.makeKey(role);
            AccessController.checkPermission(cap, Resource.ALL_ACCOUNTS, Operation.CREATE);

            IAuthenticator.create_account(name, pwd, pwd2);

            IAccount account = IAuthenticator.get_account(name);

            role = AccessController.getRole(Role.ROLE_ID.BASIC);
            AccessController.setRole(account, role);

            ReplyHelper.createReply(req, res, "Account Created");

            System.out.println("\tCreate Successful");
        } catch (UsernameAlreadyExists usernameAlreadyExists) {
            ReplyHelper.createBadReply(req, res, "Username already Exists");
            System.err.println("\tUsername already exists: " + name);
        } catch (PasswordsDoNotMatch passwordsDoNotMatch) {
            ReplyHelper.createBadReply(req, res, "Passwords do not match");
            System.err.println("\tPasswords do not match");
        } catch (InvalidInput invalidInput) {
            ReplyHelper.createBadReply(req, res, "Invalid Inputs");
            System.err.println("Invalid Inputs");
        } catch (AuthenticationError authenticationError) {
            ReplyHelper.createBadReply(req, res, "Error Authenticating User");
            System.err.println("\tError Authenticating User");
        } catch (AccessControlError accessControlError) {
            ReplyHelper.createBadReply(req, res, "Error in Access Control");
            System.err.println("\tError in Access Control");
        } catch (AccountNotFound accountNotFound) {
            accountNotFound.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }
}
