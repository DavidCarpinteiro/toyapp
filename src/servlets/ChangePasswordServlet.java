package servlets;

import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.*;
import access.*;
import session.IAuthenticator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangePasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 102831973239L;


    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT PASSWORD CHANGE");

        String pwd = req.getParameter(Configs.get(Props.password));
        String pwd_conf = req.getParameter(Configs.get(Props.password_conf));

        try {
            IAccount authUser = IAuthenticator.login(req, res);

            Role role = AccessController.getRole(authUser);
            Capability cap = AccessController.makeKey(role);
            AccessController.checkPermission(cap, Resource.SELF_ACCOUNT, Operation.MODIFY);

            IAuthenticator.change_pwd(authUser.getUsername(), pwd, pwd_conf);

            servlets.ReplyHelper.createReply(req, res, "Password Changed");

            System.out.println("\tUser Authenticated: " + authUser.getUsername());
        } catch (AuthenticationError | PasswordsDoNotMatch | AccountNotFound e) {
            ReplyHelper.createBadReply(req, res, "Incorrect Password");
            System.err.println("\tError Authenticating User");
        } catch (InvalidInput invalidInput) {
            System.err.println("Invalid Inputs");
        } catch (AccessControlError accessControlError) {
            ReplyHelper.createBadReply(req, res, "Error in Access Control");
            System.err.println("\tError in Access Control");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }
}
