package servlets;

import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.AccessControlError;
import exceptions.AccountNotFound;
import exceptions.AuthenticationError;
import access.*;
import session.IAuthenticator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 102821973239L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT GET ACCOUNT");

        String username = req.getParameter(Configs.get(Props.username));

        try {
            IAccount authUser = IAuthenticator.login(req, res);

            Role role = AccessController.getRole(authUser);
            Capability cap = AccessController.makeKey(role);

            //Check if user is requesting READ on his or others accounts
            if(authUser.getUsername().equals(username))
                AccessController.checkPermission(cap, Resource.SELF_ACCOUNT, Operation.READ);
            else
                AccessController.checkPermission(cap, Resource.ALL_ACCOUNTS, Operation.READ);

            IAccount account = IAuthenticator.get_account(username);

            String accountDisplay = "\nID: " + account.getID() +
                    "\nUsername: " + account.getUsername() +
                    "\nCreation: " + account.getCreation() +
                    "\nModified: " + account.getModified() +
                    "\nLocked: " + account.isLocked() +
                    "\nLogin: " + account.isLogin();

            ReplyHelper.createReply(req, res, accountDisplay);
            System.out.println("\tUser Authenticated: " + authUser.getUsername());
        } catch (AuthenticationError e) {
            ReplyHelper.createBadReply(req, res, "Error Authenticating User");
            System.err.println("\tError Authenticating User");
        } catch (AccountNotFound e) {
            ReplyHelper.createBadReply(req, res, "Account not found");
            System.err.println("\tAccount not found");
        } catch (AccessControlError accessControlError) {
            ReplyHelper.createBadReply(req, res, "Error in Access Control");
            System.err.println("\tError in Access Control");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }
}
