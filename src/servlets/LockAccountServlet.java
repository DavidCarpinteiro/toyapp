package servlets;

import access.*;
import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.AccessControlError;
import exceptions.AccountNotFound;
import exceptions.AuthenticationError;
import session.IAuthenticator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LockAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 102831973235L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT LOCK ACCOUNT");

        String username = req.getParameter(Configs.get(Props.username));

        try {
            IAccount authUser = IAuthenticator.login(req, res);

            Role role = AccessController.getRole(authUser);
            Capability cap = AccessController.makeKey(role);
            AccessController.checkPermission(cap, Resource.ALL_ACCOUNTS, Operation.LOCK);

            IAccount account = IAuthenticator.get_account(username);
            IAuthenticator.set_locked(account);

            ReplyHelper.createReply(req, res, "Account locked");

            System.out.println("\tUser Authenticated: " + authUser.getUsername());
        } catch (AuthenticationError e) {
            ReplyHelper.createBadReply(req, res, "Error Authenticating User");
            System.err.println("\tError Authenticating User");
        } catch (AccountNotFound e) {
            ReplyHelper.createBadReply(req, res, "Account not found");
            System.err.println("\tAccount not found");
        } catch (AccessControlError accessControlError) {
            ReplyHelper.createBadReply(req, res, "Error in Access Control");
            System.err.println("\tError in Access Control");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }
}
