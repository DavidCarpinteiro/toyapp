package servlets;

import account.IAccount;
import exceptions.AccountNotFound;
import exceptions.AuthenticationError;
import session.IAuthenticator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 101131973239L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT LOGOUT");

        try {
            IAccount authUser = IAuthenticator.login(req, res);

            IAuthenticator.logout(authUser);

            HttpSession session = req.getSession(false);

            session.invalidate();

            ReplyHelper.createReply(req, res, "Logout Successful");

            System.out.println("\tUser Authenticated: " + authUser.getUsername());
        } catch (AuthenticationError | AccountNotFound e) {
            ReplyHelper.createBadReply(req, res, "Error Authenticating User");
            System.err.println("\tError Authenticating User");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }
}
