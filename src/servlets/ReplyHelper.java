package servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ReplyHelper {
    public static void createReply(HttpServletRequest req, HttpServletResponse res, String text) {
        res.setContentType("text/html");

        try (PrintWriter out = res.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Password Changed</title></head>");
            out.println("<body>");
            out.println("<h2>OK!!!</h2>");
            out.println("<p>" + text + "</p>");
            out.println("<a href=\"/\">HOME</a>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
            System.err.println("\tCould not create reply");
        }
    }

    public static void createBadReply(HttpServletRequest req, HttpServletResponse res, String text) {
        res.setContentType("text/html");

        try (PrintWriter out = res.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Password Incorrect</title></head>");
            out.println("<body>");
            out.println("<h2>ERROR!!!</h2>");
            out.println("<p>" + text + "</p>");
            out.println("<a href=\"/\">HOME</a>");
            out.println("</body>");
            out.println("</html>");
        } catch (IOException e) {
            System.err.println("\tCould not create bad reply");
        }
    }

}
