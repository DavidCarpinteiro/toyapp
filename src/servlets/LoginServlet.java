package servlets;

import account.IAccount;
import configs.Configs;
import configs.Props;
import exceptions.AccountIsLocked;
import exceptions.AccountNotFound;
import exceptions.AuthenticationError;
import session.IAuthenticator;
import session.JWTHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 102551973239L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        System.out.println("GOT LOGIN");

        String name = req.getParameter(Configs.get(Props.username));
        String pwd = req.getParameter(Configs.get(Props.password));

        try {
            HttpSession session = req.getSession(true);
            IAccount authUser = IAuthenticator.login(name, pwd);

            session.setAttribute(Configs.get(Props.jwt), JWTHelper.createJWT(authUser));

            ReplyHelper.createReply(req, res, "User Login Successfully");

            System.out.println("\tLogin Successful");
        } catch (AccountNotFound e) {
            ReplyHelper.createBadReply(req, res, "Error on login");
            System.err.println("\tNo Account for user: " + name);
        } catch (AccountIsLocked e) {
            ReplyHelper.createBadReply(req, res, "Error on login");
            System.err.println("\tAccount is Locked!");
        } catch (AuthenticationError e) {
            ReplyHelper.createBadReply(req, res, "Error on login");
            System.err.println("\tUsername or Password is incorrect");
        }

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        doGet(req, res);
    }

}
