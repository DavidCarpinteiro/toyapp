package configs;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class Configs {

    private static final Map<Props, String> props = new HashMap<>(20);

    // TODO Fix path was the only to get this to work, change accordingly
    private static final String LOCATION = "C:\\Users\\david\\Dropbox\\SegSoft\\ToyApp\\src\\configs\\configs.properties";

    public static String get(Props p) {
        Objects.requireNonNull(p);

        if (props.isEmpty()) load(LOCATION);

        if (props.isEmpty()) throw new RuntimeException("properties not loaded");

        String tmp = props.get(p);

        if (tmp == null) throw new RuntimeException("no property found for: " + p.name());

        return tmp;
    }

    private static void load(String location) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(location));

            for (Props p : Props.values()) {
                String tmp = (String) properties.get(p.name());

                if (tmp == null || tmp.isEmpty()) {
                    throw new RuntimeException("incomplete properties file: " + location);
                }

                props.put(p, tmp);
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("error reading properties file: " + location);
        }
    }

}
