package configs;

public enum Props {
    key_alg,
    admin,
    pass,
    username,
    password,
    password_conf,
    jwt,
    key_size,
    token_ttl
}
