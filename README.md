

// Add a new admin user at startup
IAccount acc = new Account(Configs.get(Props.admin), Configs.get(Props.pass));
Role role = AccessController.newRole(Role.ROLE_ID.ROOT);
AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.MODIFY);
AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.MODIFY);
AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.READ);
AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.CREATE);
AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.DELETE);
AccessController.grantPermission(role, Resource.ALL_ACCOUNTS, Operation.READ);
AccessController.setRole(acc, role);
instance.saveAccount(acc);
// Setup permission for basic users
role = AccessController.newRole(Role.ROLE_ID.BASIC);
AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.MODIFY);
AccessController.grantPermission(role, Resource.SELF_ACCOUNT, Operation.READ);



JVM Command:
java -Djava.security.manager -Djava.security.policy=test.policy MainClass